--- Required libraries.

-- Localised functions.
local cancel = timer.cancel
local pause = timer.pause
local performWithDelay = timer.performWithDelay
local resume = timer.resume
local find = string.find
local lower = string.lower
local random = math.random
local time = os.time

-- Localised values.

--- Class creation.
local library = {}

-- Static values.

--- Initialises this Scrappy library.
function library:init()

	-- Table to store named transitions
	self._timers = {}

	-- Loop through the transition library
	for k, v in pairs( timer ) do

		-- Check if this item is a function, and it isn't a private one, nor is it one we already have, nor the enterFrame handler.
		if type( v ) == "function" and not find( k, "_" ) and not self[ k ] and k ~= "enterFrame" then

			-- Create a wrapper for it
			self[ k ] = function( self, name )

				-- And call our new timer method
				self:_performActionOnTimer( name, v )

			end

		end

	end

end

--- Starts a new timer.
-- @param delay The delay for the iterations.
-- @param callback The function to call each iteration.
-- @param iterations The number of iterations.
-- @param name The name of the new timer. Optional, defaults to 'timer' followed by a random number.
-- @return The name of the new timer.
function library:performWithDelay( delay, callback, iterations, name )

	-- Was a name passed in?
	if name then

		-- Then try to cancel an existing by that name
		self:cancel( name )

	end

	-- Do we have a name? If not, create a new one
	if not name then

		-- Function to create a new name
		local newName = function()
			return "timer" .. random( time() )
		end

		-- Create the new name
		name = newName()

		-- Does it already exist?
		while self:get( name ) do

			-- Get another name
			name = newName()

		end

	end

	-- Now create the new timer
	local t = performWithDelay( delay, callback, iterations )

	-- And add it to the list
	self._timers[ name ] = t

	-- And return the name
	return name

end

--- Performs an action on a timer.
-- @param name The name, or timerID if created without one, of the timer. Nil if to act on ALL timers.
-- @param f The function to use. Valid options are timer.cancel, timer.pause, and timer.resume.
function library:_performActionOnTimer( name, f )

	-- Do we have a valid function?
	if f and type( f ) == "function" and ( f == cancel or f == pause or f == resume ) then

		-- Do we have a name?
		if name then

			-- Get the timer
			local t = self:get( name )

			-- Do we have it?
			if t then

				-- Act on it
				f( t )

				-- Were we cancelling?
				if f == cancel then
					
					-- Nil out local
					t = nil

					-- And nil out table ref
					self._timers[ name ] = nil

				end

			end

		-- Otherwise
		else

			-- Loop through each timer
			for k, v in pairs( self._timers ) do

				-- Act on it
				self:_performActionOnTimer( k, f )

				-- Were we cancelling?
				if f == cancel then

					-- Nil out out
					self._timers[ k ] = nil

				end

			end

			-- Were we cancelling?
			if f == cancel then

				-- And clear the timer table
				self._timers = {}

			end

		end

	end

end

--- Gets a timer.
-- @param name The name of the timer
-- @return The timer id, nil if none found.
function library:get( name )
	return self._timers[ name ]
end

--- Sets whether allowInteratonsWithinFrame is true or not.
-- @param value The value to set.
function library:allowInteratonsWithinFrame( value )
	timer.allowInteratonsWithinFrame = value
end

--- Checks whether allowInteratonsWithinFrame is true or not.
-- @return True if they are, false otherwise.
function library:areInteratonsWithinFrameAllowed()
	return timer.allowInteratonsWithinFrame
end

--- Destroys this Scrappy Library.
function library:destroy()

	-- Cancel all transitions
	self:cancel()

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Timer library
if not Scrappy.Timer then

	-- Then store the library out
	Scrappy.Timer = library

end

-- Return the new library
return library
